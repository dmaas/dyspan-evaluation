#!/bin/bash
set -ex
BINDIR=`dirname $0`
source $BINDIR/common.sh

if [ -f $SRCDIR/gnuradio-setup-complete ]; then
    echo "setup already ran; not running again"
    exit 0
fi

while ! wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
do
    echo Failed to get emulab key, retrying
done

while ! sudo add-apt-repository -y http://repos.emulab.net/powder/ubuntu/
do
    echo Failed to get johnsond ppa, retrying
done

while ! sudo apt-get update
do
    echo Failed to update, retrying
done

sudo apt install -y
    libuhd-dev \
    uhd-host
    git \
    cmake \
    g++ \
    libboost-all-dev \
    libgmp-dev \
    swig \
    python3-numpy \
    python3-mako \
    python3-sphinx \
    python3-lxml \
    doxygen \
    libfftw3-dev \
    libsdl1.2-dev \
    libgsl-dev \
    libqwt-qt5-dev \
    libqt5opengl5-dev \
    python3-pyqt5 \
    liblog4cpp5-dev \
    libzmq3-dev \
    python3-yaml \
    python3-click \
    python3-click-plugins \
    python3-zmq \
    python3-scipy \
    python3-gi \
    python3-gi-cairo gir1.2-gtk-3.0 \
    libcodec2-dev \
    libgsm1-dev

cd $SRCDIR
git clone --recursive https://github.com/gnuradio/volk.git
cd volk
mkdir build
cd build
cmake ../
make -j`nproc`
sudo make install
sudo ldconfig

cd $SRCDIR
git clone https://github.com/gnuradio/gnuradio.git
cd gnuradio
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ../
make -j`nproc`
sudo make install

touch $SRCDIR/gnuradio-setup-complete
