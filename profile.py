#!/usr/bin/env python
import hashlib
import random
import os

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab as emulab
import geni.rspec.emulab.ansible

from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)
from lxml import etree as ET


tourDescription = """
### DySPAN Evaluation

"""


HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"
FIXBS = "sudo /local/repository/fixbs.sh"
BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
OLD_GRUHD_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"
UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
DEFAULT_SRSRAN_HASH = "5e6f50a202c6efa671d5b231d7c911dc6c3d86ed"
OPEN5GS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-open5gs.sh")
SRSRAN_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-srsran.sh")
GNURADIO_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-gnuradio.sh")
DESIRED_FREQ_LOW_MHZ = 3360.0
DESIRED_FREQ_HIGH_MHZ = 3600.0
DESIRED_BANDWIDTH_MHZ = 20.0
DESIRED_TX_POWER_DBM = 0.0
SCOS_NAME = "scos"
SCOS_NODE_ID = "nuc16"
NODE_IDS = {
    "int": ["nuc8", "nuc9", "nuc10"],
    SCOS_NAME: SCOS_NODE_ID,
    "gnb": "nuc23",
    "ue": "nuc27",
    "ota_radios": ["cnode-{}".format(name) for name in ["wasatch","mario","moran","guesthouse","ebc","ustar"]],
}
INTERFERENCE_NODES = ["int_{}".format(index) for index in range(len(NODE_IDS["int"]))]
MATRIX_GRAPH = {
    "gnb": ["ue", SCOS_NAME],
    SCOS_NAME: ["gnb"] + INTERFERENCE_NODES,
    "ue": ["gnb"] + INTERFERENCE_NODES,
}
for interference_node in INTERFERENCE_NODES:
    MATRIX_GRAPH[interference_node] = [SCOS_NAME, "ue"]

MATRIX_INPUTS = ["gnb"] + INTERFERENCE_NODES
RF_IFACES = {}
RF_LINK_NAMES = {}
for k, v in MATRIX_GRAPH.items():
    RF_IFACES[k] = {}
    for node in (v):
        RF_IFACES[k][node] = "{}_{}_rf".format(k, node)
        if k in MATRIX_INPUTS:
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)] = []


for k, v in MATRIX_GRAPH.items():
    if k in MATRIX_INPUTS:
        for node in (v):
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)].append(RF_IFACES[k][node])
            RF_LINK_NAMES["rflink_{}_{}".format(k, node)].append(RF_IFACES[node][k])


pc = portal.Context()
node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]

pc.defineParameter(
    name="cn_nodetype",
    description="Type of compute node to use for CN node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types,
    advanced=False,
)

pc.defineParameter(
    name="monitor_nodetype",
    description="Type of compute node to use for X310 node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types,
    advanced=False,
)

pc.defineParameter(
    name="srsran_commit_hash",
    description="Commit hash for srsRAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True,
)

portal.context.defineStructParameter(
    "desired_freq_ranges", "Frequency ranges and bandwidths to request (MHz)",
    defaultValue=[{
        "freq_min": DESIRED_FREQ_LOW_MHZ,
        "freq_max": DESIRED_FREQ_HIGH_MHZ,
        "tx_power": DESIRED_TX_POWER_DBM,
        "bandwidth": DESIRED_BANDWIDTH_MHZ,
    }],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges/bandwidths to request for transmission.",
    members=[
        portal.Parameter(
            "freq_min",
            "Lowest desired frequency (MHz)",
            portal.ParameterType.BANDWIDTH,
            DESIRED_FREQ_LOW_MHZ,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Highest desired frequency (MHz)",
            portal.ParameterType.BANDWIDTH,
            DESIRED_FREQ_HIGH_MHZ,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "tx_power",
            "Desired transmit power (dBm)",
            portal.ParameterType.BANDWIDTH,
            DESIRED_TX_POWER_DBM
        ),
        portal.Parameter(
            "bandwidth",
            "Desired bandwidth (MHz)",
            portal.ParameterType.BANDWIDTH,
            DESIRED_BANDWIDTH_MHZ
        ),
    ]
)

portal.context.defineStructParameter(
    "outdoor_radios", "Outdoor radios to allocate (for testing geoqueries).",[],
    multiValue=True, itemDefaultValue={}, min=0, max=None,
    members=[
        portal.Parameter(
            "radio_id", "Radio",
            portal.ParameterType.STRING, NODE_IDS["ota_radios"][0],
            NODE_IDS["ota_radios"]
        ),
    ]
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

node_name = "cn5g"
cn_node = request.RawPC(node_name)
cn_node.component_manager_id = COMP_MANAGER_ID
cn_node.hardware_type = params.cn_nodetype
cn_node.disk_image = UBUNTU_IMG
cn_if = cn_node.addInterface("cn-if")
cn_if.addAddress(rspec.IPv4Address("192.168.1.1", "255.255.255.0"))
cn_link = request.Link("cn-link")
cn_link.setNoBandwidthShaping()
cn_link.addInterface(cn_if)
cn_node.addService(rspec.Execute(shell="bash", command=OPEN5GS_DEPLOY_SCRIPT))

node_name = "monitor"
monitor = request.RawPC(node_name)
monitor.component_manager_id = COMP_MANAGER_ID
monitor.hardware_type = params.monitor_nodetype
monitor.disk_image = OLD_GRUHD_IMG
monitor_usrp_if = monitor.addInterface("monitor-usrp-if")
monitor_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
node_name = "monitor-sdr"
monitor_sdr = request.RawPC(node_name)
monitor_sdr.component_manager_id = COMP_MANAGER_ID
monitor_sdr.component_id = "oai-wb-b1"
monitor_sdr_if = monitor_sdr.addInterface("monitor-sdr-if")
monitor_sdr_link = request.Link("monitor-sdr-link")
monitor_sdr_link.bandwidth = 10*1000*1000
monitor_sdr_link.addInterface(monitor_usrp_if)
monitor_sdr_link.addInterface(monitor_sdr_if)
monitor.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

node_name = "interferer"
interferer = request.RawPC(node_name)
interferer.component_manager_id = COMP_MANAGER_ID
interferer.hardware_type = params.monitor_nodetype
interferer.disk_image = OLD_GRUHD_IMG
interferer_usrp_if = interferer.addInterface("interferer-usrp-if")
interferer_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
node_name = "interferer-sdr"
interferer_sdr = request.RawPC(node_name)
interferer_sdr.component_manager_id = COMP_MANAGER_ID
interferer_sdr.component_id = "oai-wb-b2"
interferer_sdr_if = interferer_sdr.addInterface("interferer-sdr-if")
interferer_sdr_link = request.Link("interferer-sdr-link")
interferer_sdr_link.bandwidth = 10*1000*1000
interferer_sdr_link.addInterface(interferer_usrp_if)
interferer_sdr_link.addInterface(interferer_sdr_if)
interferer.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

matrix_nodes = {}
node_name = "gnb"
gnb = request.RawPC(node_name)
gnb.component_manager_id = COMP_MANAGER_ID
gnb.component_id = NODE_IDS[node_name]
gnb.disk_image = UBUNTU_IMG
gnb_cn_if = gnb.addInterface("gnb-cn-if")
gnb_cn_if.addAddress(rspec.IPv4Address("192.168.1.2", "255.255.255.0"))
cn_link.addInterface(gnb_cn_if)
gnb.Desire("rf-controlled", 1)
if params.srsran_commit_hash:
    srsran_hash = params.srsran_commit_hash
else:
    srsran_hash = DEFAULT_SRSRAN_HASH
cmd = "{} '{}'".format(SRSRAN_DEPLOY_SCRIPT, srsran_hash)
gnb.addService(rspec.Execute(shell="bash", command=cmd))
matrix_nodes[node_name] = gnb

for ind, node_name in enumerate(INTERFERENCE_NODES):
    node = request.RawPC(node_name)
    node.component_manager_id = COMP_MANAGER_ID
    node.component_id = NODE_IDS["int"][ind]
    node.disk_image = OLD_GRUHD_IMG
    node.Desire("rf-controlled", 1)
    # node.addService(rspec.Execute(shell="bash", command=GNURADIO_DEPLOY_SCRIPT))
    matrix_nodes[node_name] = node

for radio in params.outdoor_radios:
    node = request.RawPC("ota-{}".format(radio.radio_id))
    node.component_manager_id = COMP_MANAGER_ID
    node.component_id = radio.radio_id
    node.disk_image = OLD_GRUHD_IMG

node_name = SCOS_NAME
scos = request.RawPC(node_name)
scos.component_manager_id = COMP_MANAGER_ID
scos.component_id = NODE_IDS[node_name]
scos.disk_image = UBUNTU_IMG
request.addRole(
    Role(
        name="scos-sensor",
        path="ansible",
        playbooks=[Playbook("scos-sensor", path="scos-sensor.yml", become="root")],
    )
)
scos.bindRole(RoleBinding("scos-sensor"))
scos.addService(rspec.Execute(shell="sh", command=FIXBS))
scos.addService(rspec.Execute(shell="sh", command=HEAD_CMD))
bs = scos.Blockstore(node_name + "-bs", "/opt")
bs.size = "0GB"
scos.Desire("rf-controlled", 1)
matrix_nodes[node_name] = scos
request.addResource(IG.Password("perExptPassword"))
request.addOverride(
    Override(
        name="scos_sensor_admin_password",
        source="password",
        source_name="perExptPassword",
    )
)

node_name = "ue"
ue = request.RawPC(node_name)
ue.component_manager_id = COMP_MANAGER_ID
ue.component_id = NODE_IDS[node_name]
ue.disk_image = COTS_UE_IMG
ue.Desire("rf-controlled", 1)
matrix_nodes[node_name] = ue

rf_ifaces = {}
for node_name, node in matrix_nodes.items():
    for rf_iface_name in RF_IFACES[node_name].values():
        rf_ifaces[rf_iface_name] = node.addInterface(rf_iface_name)

for rf_link_name, rf_iface_names in RF_LINK_NAMES.items():
    rf_link = request.RFLink(rf_link_name)
    for iface_name in rf_iface_names:
        rf_link.addInterface(rf_ifaces[iface_name])

for frange in params.desired_freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, frange.tx_power, frange.bandwidth)

tourInstructions = """

Once your experiment completes setup, you will be able to access the [scos-sensor web interface](https://{host-""" + SCOS_NAME + """-""" + SCOS_NODE_ID + """}/) (user `admin`, password `{password-perExptPassword}`).

Note: this profile includes startup scripts that download, install, and
configure the required software stacks. After the experiment becomes ready, wait
until the "Startup" column on the "List View" tab indicates that the startup
scripts have finished on all of the nodes before proceeding.

#### Overview

#### Instructions

"""

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
